//
//  TabbarVC.m
//  LitCircle
//
//  Created by Afzal Sheikh on 12/14/16.
//  Copyright © 2016 Afzal Sheikh. All rights reserved.
//

#import "TabbarVC.h"

@interface TabbarVC ()

@end

@implementation TabbarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITabBarItem *item = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *item1 = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *item2 = [self.tabBar.items objectAtIndex:2];
    UITabBarItem *item3 = [self.tabBar.items objectAtIndex:3];
    UITabBarItem *item4 = [self.tabBar.items objectAtIndex:4];
    
    
    
    item.title = @"Home";
    item1.title = @"Search";
    item2.title = @"Post";
    item3.title = @"Notifications";
    item4.title = @"Profile";
    
    
    
    // this way, the icon gets rendered as it is (thus, it needs to be green in this example)
    item.image = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item1.image = [[UIImage imageNamed:@"searchIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item2.image = [[UIImage imageNamed:@"posticon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item3.image = [[UIImage imageNamed:@"notificationicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    item4.image = [[UIImage imageNamed:@"profileicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    
    [item setSelectedImage:[[UIImage imageNamed:@"homeselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item1 setSelectedImage:[[UIImage imageNamed:@"searchSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item2 setSelectedImage:[[UIImage imageNamed:@"postSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item3 setSelectedImage:[[UIImage imageNamed:@"notificationSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [item4 setSelectedImage:[[UIImage imageNamed:@"profileSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
